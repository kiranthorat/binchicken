import Foundation
import UIKit

enum ItemType {
    case yellow
    case red
}

struct ItemDetails {
    let name: String
    let type: ItemType
}

class BinDetailsViewController: PullUpController {

    var itemDetails: ItemDetails = ItemDetails(name: "", type: .yellow)
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var imageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        switch itemDetails.type {
        case .yellow:
            imageView.image = #imageLiteral(resourceName: "Yellow-Background")
            label.text = "Put \(itemDetails.name) in Yellow Bin"
        default:
            imageView.image = #imageLiteral(resourceName: "Red-Background")
            label.text = "Put \(itemDetails.name) in Red Bin"
        }
    }

    override open var pullUpControllerPreviewOffset: CGFloat {
        return 150
    }

    override open var pullUpControllerPreferredSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 500)
    }

    @objc
    @IBAction func dismiss() {
        removePullUpController(self, animated: true)
    }
}
