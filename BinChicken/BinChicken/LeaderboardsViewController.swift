import UIKit
    
class LeaderboardsViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var segmentedView: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        var size = segmentedView.frame.size
        size.height = 50
        segmentedView.frame.size = size
        self.title = "Impact"
    }

    @objc
    @IBAction func segmentClicked() {

        if segmentedView.selectedSegmentIndex == 0 {
            imageView.image = #imageLiteral(resourceName: "My Contributions")
        } else {
            imageView.image = #imageLiteral(resourceName: "Leaderboard")
        }
    }
}
