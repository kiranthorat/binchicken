import UIKit
import AVFoundation
import Foundation
import Vision

class ViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
    // video capture session
    let session = AVCaptureSession()
    // preview layer
    var previewLayer: AVCaptureVideoPreviewLayer!
    // queue for processing video frames
    let captureQueue = DispatchQueue(label: "captureQueue")
    // overlay layer
    var gradientLayer: CAGradientLayer!
    // vision request
    var visionRequests = [VNRequest]()

    var recognitionThreshold : Float = 0.70

    var itemDetails: ItemDetails = ItemDetails(name: "", type: .yellow)

    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var resultView: UILabel!
    @IBOutlet weak var toggleButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // get hold of the default video camera
        self.navigationController?.navigationBar.prefersLargeTitles = true

        guard let camera = AVCaptureDevice.default(for: .video) else {
            fatalError("No video camera available")
        }

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.toggleSession))
        tapGestureRecognizer.numberOfTapsRequired = 2
        self.view.addGestureRecognizer(tapGestureRecognizer)

        do {
            // add the preview layer
            previewLayer = AVCaptureVideoPreviewLayer(session: session)
            previewView.layer.addSublayer(previewLayer)
            // add a slight gradient overlay so we can read the results easily
            gradientLayer = CAGradientLayer()
            gradientLayer.colors = [
                UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.7).cgColor,
                UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.0).cgColor,
            ]
            gradientLayer.locations = [0.0, 0.3]
            self.previewView.layer.addSublayer(gradientLayer)

            // create the capture input and the video output
            let cameraInput = try AVCaptureDeviceInput(device: camera)

            let videoOutput = AVCaptureVideoDataOutput()
            videoOutput.setSampleBufferDelegate(self, queue: captureQueue)
            videoOutput.alwaysDiscardsLateVideoFrames = true
            videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA]
            session.sessionPreset = .high

            // wire up the session
            session.addInput(cameraInput)
            session.addOutput(videoOutput)

            // make sure we are in portrait mode
            let conn = videoOutput.connection(with: .video)
            conn?.videoOrientation = .portrait

            // Start the session
            session.startRunning()

            // set up the vision model
            guard let resNet50Model = try? VNCoreMLModel(for: ImageClassifier().model) else {
                fatalError("Could not load model")
            }
            // set up the request using our vision model
            let classificationRequest = VNCoreMLRequest(model: resNet50Model, completionHandler: handleClassifications)
            classificationRequest.imageCropAndScaleOption = .centerCrop
            visionRequests = [classificationRequest]
        } catch {
            fatalError(error.localizedDescription)
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        previewLayer.frame = self.previewView.bounds;
        gradientLayer.frame = self.previewView.bounds;

        let orientation: UIDeviceOrientation = UIDevice.current.orientation;
        switch (orientation) {
        case .portrait:
            previewLayer?.connection?.videoOrientation = .portrait
        case .landscapeRight:
            previewLayer?.connection?.videoOrientation = .landscapeLeft
        case .landscapeLeft:
            previewLayer?.connection?.videoOrientation = .landscapeRight
        case .portraitUpsideDown:
            previewLayer?.connection?.videoOrientation = .portraitUpsideDown
        default:
            previewLayer?.connection?.videoOrientation = .portrait
        }
    }

    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }

        connection.videoOrientation = .portrait

        var requestOptions:[VNImageOption: Any] = [:]

        if let cameraIntrinsicData = CMGetAttachment(sampleBuffer, key: kCMSampleBufferAttachmentKey_CameraIntrinsicMatrix, attachmentModeOut: nil) {
            requestOptions = [.cameraIntrinsics: cameraIntrinsicData]
        }

        // for orientation see kCGImagePropertyOrientation
        let imageRequestHandler = VNImageRequestHandler(cvPixelBuffer: pixelBuffer, orientation: .upMirrored, options: requestOptions)
        do {
            try imageRequestHandler.perform(self.visionRequests)
        } catch {
            print(error)
        }
    }

    @IBAction func toggleSession(sender: Any) {

        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "BinDetailsView") as? BinDetailsViewController {
            viewController.itemDetails = itemDetails
            addPullUpController(viewController, animated: true)
        }
    }

    func handleClassifications(request: VNRequest, error: Error?) {
        if let theError = error {
            print("Error: \(theError.localizedDescription)")
            return
        }
        guard let observation = request.results?.first as? VNClassificationObservation else {
            print("No results")
            return
        }

        //print(observations)

//        let classifications = observations[0...4] // top 4 results
//            .compactMap({ $0 as? VNClassificationObservation })
//            .compactMap({$0.confidence > recognitionThreshold ? $0 : nil})
//            .map({ "\($0.identifier) \(String(format:"%.2f", $0.confidence))" })
//            .joined(separator: "\n")

        let displayName: String?
        let itemType: ItemType?

        let color: UIColor
        let yellowColor = #colorLiteral(red: 0.9529411765, green: 0.9254901961, blue: 0.4901960784, alpha: 0.8)

        switch observation.identifier {
        case "cardboard":
            displayName = "Cardboard"
            itemType = ItemType.yellow
            color = yellowColor
        case "glass":
            displayName = "Glass"
            itemType = ItemType.yellow
            color = yellowColor
        case "metal":
            displayName = "Metal"
            itemType = ItemType.yellow
            color = yellowColor
        case "paper":
            displayName = "Paper"
            itemType = ItemType.yellow
            color = yellowColor
        case "plastic":
            displayName = "Plastic"
            itemType = ItemType.yellow
            color = yellowColor
        case "trash":
            displayName = "Trash"
            color = #colorLiteral(red: 0.9098039216, green: 0.2784313725, blue: 0.2509803922, alpha: 0.5)
            itemType = ItemType.red
        default:
            displayName = nil
            itemType = nil
            color = #colorLiteral(red: 0.09803921569, green: 0.768627451, blue: 0.5137254902, alpha: 0.5)
        }

        guard let name = displayName, let type = itemType else {
            return
        }

        itemDetails = ItemDetails(name: name, type: type)

        DispatchQueue.main.async {
            self.resultView.text = displayName
            self.container.backgroundColor = color
        }
    }
}
